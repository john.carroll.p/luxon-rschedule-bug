import MockDate from 'mockdate'; // method for setting client's local time
import { DateTime } from 'luxon';

describe('create a date in the BST->GMT boundary', () => {
  function runLuxonTest() {
    const date = DateTime.fromObject({
      year: 2021,
      month: 10,
      day: 31,
      hour: 1,
      minute: 45,
    }, {
      zone: 'Europe/London',
    });

    expect(date.toJSDate().toISOString()).toEqual('2021-10-31T00:45:00.000Z');
  }

  it('when the local time is before the DST change', () => {
    MockDate.set(new Date(2021, 9, 29));
    runLuxonTest();
    MockDate.reset();
  });

  it('when the local time is after the DST change', () => {
    MockDate.set(new Date(2021, 10, 4));
    runLuxonTest();
    MockDate.reset();
  });
});
